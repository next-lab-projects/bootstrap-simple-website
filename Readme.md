# HTML Form Handling Practice Project

## Project Screenshot

> ![SS](./ss.png)

---

## Web Vitals Report

> ![SS](./webVitals1.png)

---

> ![SS](./webVitals2.png)

---

## You can Check it Live on Below Link :

> [![Live Link](https://img.shields.io/badge/DEPLOYED-LINK-green)](https://bootstrap-simple-website-vp.netlify.app/)

---
